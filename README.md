Welcome home! Cottonwood Apartments features eight spacious floor plans with one, two, and three bedroom layouts. Amenities include full-size washer / dryer connections and spacious walk in closets. The property features a boutique pool, outdoor fire pit.

Address: 974 Olympus Park Dr, Salt Lake City, UT 84117, USA

Phone: 801-784-9584
